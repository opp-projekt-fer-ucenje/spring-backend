package hr.fer.ffhan.ucenje;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UcenjeApplication {
    public static void main(String[] args) {
        SpringApplication.run(hr.fer.ffhan.ucenje.UcenjeApplication.class, args);
    }
}
