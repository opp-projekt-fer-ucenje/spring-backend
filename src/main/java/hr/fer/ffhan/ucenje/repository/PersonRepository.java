package hr.fer.ffhan.ucenje.repository;

import hr.fer.ffhan.ucenje.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    List<Person> getAllByAge(int age);

    Person getById(Long id);

    List<Person> getByName(String name);

    boolean existsById(Long id);
}