package hr.fer.ffhan.ucenje.model;

import hr.fer.ffhan.ucenje.model.dto.PersonDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// generira gettere i settere
@Data
// definira da postoji u bazi podataka
@Entity
// generira prazan konstruktor
@NoArgsConstructor
// generira konstruktor sa svim parametrima
@AllArgsConstructor
// generira builder pattern
@Builder
public class Person {
    // definiramo da je ovo ID u tablici u bazi podataka
    @Id
    // definiramo strategiju generiranja ID-a u tablici - potrebno jer inače hibernate neće znati kako generirati ID za novi red u tablici
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private int age;

    /**
     * Kreira model iz dobivenog DTO objekta.
     * ID se automatski generira u hibernate sloju prilikom spremanja objekta.
     * @param personDto: DTO objekt
     */
    public static Person packageFromDto(PersonDto personDto) {
        // koristimo builder jer među generiranim konstruktorima nema verzije bez id-a.
        return Person.builder()
                .name(personDto.getName())
                .age(personDto.getAge())
                .build();
    }
}
