package hr.fer.ffhan.ucenje.model.dto;

import hr.fer.ffhan.ucenje.model.Person;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO (Data transfer object) su objekti korišteni kao most između sučelja koje je izloženo korisniku aplikacije i
 * finalnim slojevima baze podataka. Služe da zaštite određena polja (npr ID) od vanjskih korisnika i da nemaju pristup njima
 * obično se korisniku ne vraća model kojeg koristimo direktno u komunikaciji s bazom, nego prepakirani DTO objekt koji
 * sadrži samo informacije koje želimo da korisnik može vidjeti.
 */
// generira gettere i settere
@Data
// generira prazan konstruktor
@NoArgsConstructor
// generira konstruktor sa svim parametrima
@AllArgsConstructor
// generira builder pattern
@Builder
public class PersonDto {
    private String name;
    private int age;

    /**
     * Čisti podatke o osobi za vanjsku uporabu i kreira novi DTO objekt
     * @param person: Person model kojeg želimo osigurati za vanjsku uporabu
     */
    public static PersonDto packageFromModel(Person person) {
        return new PersonDto(person.getName(), person.getAge());
    }
}
