package hr.fer.ffhan.ucenje.service;

import hr.fer.ffhan.ucenje.model.Person;
import hr.fer.ffhan.ucenje.model.dto.PersonDto;
import hr.fer.ffhan.ucenje.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "userService")
public class PersonService {

    private PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public boolean registerPerson(PersonDto personDto) {
        // prepakiravamo DTO u model za bazu podataka - strogo kontroliramo kako se kreira model Person
        Person person = Person.packageFromDto(personDto);
        // vraćamo true ako je Person uspješno spremljen
        return personRepository.save(person) != null;
    }
    public List<Person> getAll() {
        return personRepository.findAll();
    }
    public Person getById(Long id) {
        return personRepository.getById(id);
    }
    public List<Person> getByName(String name) {
        return personRepository.getByName(name);
    }

}
