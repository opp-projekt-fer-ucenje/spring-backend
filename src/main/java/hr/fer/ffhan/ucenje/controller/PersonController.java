package hr.fer.ffhan.ucenje.controller;

import hr.fer.ffhan.ucenje.model.Person;
import hr.fer.ffhan.ucenje.model.dto.PersonDto;
import hr.fer.ffhan.ucenje.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Definiramo RESTful kontroler koji korisniku otvara endpoint API-ja da vidi sve registrirane korisnike,
 * filtrira ih po ID-u, imenu te da registrira korisnika.
 *
 * Registracija ide po POST requestu, po specifikaciji HTTP-a
 * Filtriranje i općenito traženje postojećih resursa ide po GET requestu.
 *
 * \@GetMapping daje do znanja da metodu želimo otvoriti GET requestovima
 * \@PostMapping analogno za POST request
 *
 * params parametar unutar anotacija za mapiranje odvaja različite metode istih potpisa (ista metoda i url) i potrebna je
 * u ovakvim slučajevima
 *
 * ResponseEntity je wrapper koji nam omogućava da nam sve metode imaju uniforman potpis povratnih vrijednosti.
 * Također omogućava da specifiramo HTTP statusne kodove u slučaju da nešto nije prošlo dobro.
 */
@RestController
public class PersonController {

    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public ResponseEntity index() {
        return ResponseEntity.ok(personService.getAll());
    }

    @GetMapping(params = {"name"})
    public ResponseEntity getByName(@RequestParam String name) {
        return ResponseEntity.ok(personService.getByName(name));
    }

    @GetMapping(params = {"id"})
    public ResponseEntity getById(@RequestParam Long id) {
        Person person = personService.getById(id);
        if (person != null) {
            return ResponseEntity.ok(person);
        }
        return ResponseEntity.badRequest().body("No such user");
    }

    @PostMapping
    public ResponseEntity registerPerson(@RequestBody PersonDto person) {
        boolean registrationSuccess = personService.registerPerson(person);
        if (registrationSuccess) {
            return ResponseEntity.ok("Successful registration");
        }
        return ResponseEntity.badRequest().body("Invalid request.");
    }

}
